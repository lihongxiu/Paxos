package com.cat.paxos.test;

import com.cat.paxos.acceptor.Acceptor;
import com.cat.paxos.acceptor.ConfirmProposalResponse;
import com.cat.paxos.acceptor.ObjectAcceptor;
import com.cat.paxos.acceptor.ReceiveProposalResponse;
import com.cat.paxos.learner.*;
import com.cat.paxos.proposal.Proposal;
import com.cat.paxos.proposer.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @auther Cat.wang
 * @date 2020/1/17 17:29
 */
public class Test {

    @org.junit.Test
    public void test() throws PrepareProposalException, CommitProposalException {

        // 创建学习者
        List<Learner> learners = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            learners.add(new MemoryLearner());
        }
        for (Learner learner : learners) {
            learner.others().clear();
            learner.others().addAll(learners.stream().filter(l -> l != learner).map(l -> new LearnerProxy(l)).collect(Collectors.toList()));
        }

        // 创建接收者
        List<Acceptor> acceptors = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Acceptor acceptor = new ObjectAcceptor();
            acceptor.learners().clear();
            acceptor.learners().addAll(learners);
            acceptors.add(acceptor);
        }

        // 创建提案者
        List<Proposer> proposers = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Proposer proposer = new ObjectProposer();
            proposer.acceptors().addAll(
                    acceptors.stream()
                            .map(a -> new AcceptorProxy(a))
                            .collect(Collectors.toList())
            );
            proposers.add(proposer);
        }

        Proposer proposer = proposers.get(0);

        for (int i = 0; i < 1000; i++) {
            Proposal<String> p = proposer.prepare("测试_" + i);

            for (Proposer ppr : proposers) {
                CommitProposalResponse c = ppr.commit(p);
                System.out.println(String.format("提交结果：%s，提案编号：%s", c.ok(), c.proposalNumber()));
            }
        }

        System.out.println();

    }

    class LearnerProxy implements Learner {

        private Learner target;

        public LearnerProxy(Learner target) {
            this.target = target;
        }

        @Override
        public PreviewProposalResponse preview(String proposalNumber) {
            return target.preview(proposalNumber);
        }

        @Override
        public <T> LearnProposalResponse learn(Proposal<T> proposal) {
            return target.learn(proposal);
        }

        @Override
        public List<Learner> others() {
            return target.others();
        }
    }

    class AcceptorProxy implements Acceptor {

        private Acceptor target;

        public AcceptorProxy(Acceptor target) {
            this.target = target;
        }

        @Override
        public String getProposalNumber() {
            return target.getProposalNumber();
        }

        @Override
        public <T> ReceiveProposalResponse receive(Proposal<T> proposal) {
            return target.receive(proposal);
        }

        @Override
        public ConfirmProposalResponse confirm(String proposalNumber) {
            return target.confirm(proposalNumber);
        }

        @Override
        public List<Learner> learners() {
            return target.learners();
        }

        @Override
        public LeanerStrategy strategy() {
            return target.strategy();
        }
    }

}
