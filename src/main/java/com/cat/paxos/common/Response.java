package com.cat.paxos.common;

/**
 * 返回对象
 * @auther Cat.wang
 * @date 2020/1/15 15:48
 */
public interface Response {

    /**
     * 是否成功
     * @return
     */
    boolean ok();

}
