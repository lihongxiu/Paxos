package com.cat.paxos.proposal;

/**
 * @auther Cat.wang
 * @date 2020/1/15 16:07
 */
public class ObjectProposal implements Proposal<Object> {

    /**
     * 提案编号
     */
    private String number;

    /**
     * 负载
     */
    private Object payload;

    /**
     * 负载序列化器
     */
    private PayloadSerializer<Object> serializer = new ObjectPayloadSerializer();

    public ObjectProposal(String number, Object payload) {
        this.number = number;
        this.payload = payload;
    }

    @Override
    public String number() {
        return this.number;
    }

    @Override
    public Object payload() {
        return this.payload;
    }

    @Override
    public PayloadSerializer<Object> serializer() {
        return this.serializer;
    }

    public void setSerializer(PayloadSerializer<Object> serializer) {
        this.serializer = serializer;
    }
}
