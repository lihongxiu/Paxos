package com.cat.paxos.proposal;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * @auther Cat.wang
 * @date 2020/1/15 17:33
 */
public class ProposalNumberComparator implements Comparator<String> {

    /**
     * 默认的提案编号比较器
     */
    public static final ProposalNumberComparator DEFAULT_PROPOSAL_NUMBER_COMPARATOR = new ProposalNumberComparator();

    @Override
    public int compare(String o1, String o2) {
        return new BigDecimal(o1).compareTo(new BigDecimal(o2));
    }
}
