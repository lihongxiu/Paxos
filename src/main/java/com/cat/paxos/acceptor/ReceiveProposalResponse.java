package com.cat.paxos.acceptor;

import com.cat.paxos.common.Response;

/**
 * 接收提案的回应
 * @auther Cat.wang
 * @date 2020/1/15 15:30
 */
public interface ReceiveProposalResponse extends Response {

    /**
     * 获取提案编号 成功返回当前的提案编号，失败返回接收的最大提案编号
     * @return
     */
    String proposalNumber();

}
