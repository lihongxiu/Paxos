# Paxos

#### 介绍
利用Paxos算法实现分布式数据一致性

#### 软件架构
软件架构说明  
1. acceptor 提案接收者，用于接收提案，成功接收提案后发给学习者学习
2. learner 学习提案接收者的提案，并用于互相传播学习失败的提案
3. proposal 提案，代表提案对象，包含提案编号和传播的负载数据
4. proposer 提案提交者，用于提交提案，创建提案并提交给提交者

#### 内容说明
proposer根据acceptor的最大提案编号创建proposal并提交给acceptor。  
acceptor接收后校验是否是可接受的提案（编号大于或等于提案最大提案编号）。  
若不能接受返回失败的结果，若能接受返回接收成功的结果。  
proposer收到acceptor接收成功的结果后，并确认提交此结果。  
acceptor收到提案确认的请求后，先将编号提交给learner学习。  
learner收到学习请求后会先将提案编号广播给所有learner预览，若预览成功则进行学习并传播给其它learner学习。

#### 使用说明

1. 请查看测试代码：com.cat.paxos.test.Test.test

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)